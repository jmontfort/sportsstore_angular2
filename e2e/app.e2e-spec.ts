import { SportsStoreAngular2Page } from './app.po';

describe('sports-store-angular2 App', function() {
  let page: SportsStoreAngular2Page;

  beforeEach(() => {
    page = new SportsStoreAngular2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
