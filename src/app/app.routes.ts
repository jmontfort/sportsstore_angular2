import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import { HomeComponent } from '../app/components/home/home.component';
import { LoginComponent } from '../app/components/login/login.component';
import { LoggedInGuard } from '../app/logged-in.guard';
import { CategoriesComponent} from '../app/components/categories/categories.component';
import { BrandsComponent} from '../app/components/brands/brands.component';
import { ProductsComponent } from '../app/components/products/products.component';
import { UsersComponent } from '../app/components/users/users.component';
import { LogoutComponent } from '../app/components/logout/logout.component';
import { RegisterComponent} from '../app/components/register/register.component';
import { NotAuthorizedComponent} from '../app/components/not-authorized/not-authorized.component';

const appRoutes: Routes = [
    {path: '', component: HomeComponent, pathMatch: 'full', canActivate: [LoggedInGuard]},
    {path: 'login', component: LoginComponent},
    {path: 'register', component: RegisterComponent},
    {path: 'notAuthorized', component: NotAuthorizedComponent},
    {path: 'categories', component: CategoriesComponent, canActivate: [LoggedInGuard]},
    {path: 'brands', component: BrandsComponent, canActivate: [LoggedInGuard]},
    {path: 'products', component: ProductsComponent, canActivate: [LoggedInGuard]},
    {path: 'users', component: UsersComponent, canActivate: [LoggedInGuard]},
    {path: 'logout', component: LogoutComponent, canActivate: [LoggedInGuard]},
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);   