import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs/Rx';

import { CategoriesService } from '../../services/categories.service';
import { Category } from '../../Classes/Category';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {
  categories: Category[] = [];
  searchStr: string;

  constructor(private _categoriesService:CategoriesService) { }

  ngOnInit() {
    this.getCategories();
  }

  getCategories() {
     this._categoriesService.getCategories()
      .subscribe( res => {
         this.categories = res;
    })
  }

    searchCategories() {
        this._categoriesService.search(this.searchStr)
          .subscribe(res => {
            this.categories = res;
          })
    }

    editCategory(id:number){
      this._categoriesService.checkExistingProducts(id)
          .subscribe(res => {
            if(res.id > 0){
              this.confirmBox();
            }
          })
    }

    confirmBox(){
        //var c = confirm("Are you sure you want to do that?");
        //var status = document.getElementById("content");
        let myDialog:any = <any>document.getElementById("content");
          myDialog.showModal();
        // if (c == true)
        // {     
        //     //show the edit popup
        // }
        // else
        // {
            
        // }
    }
}
