import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {UserService} from 'app/services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  errorMessage: string = "";

  constructor(private _userService:UserService, private _router:Router) { }

  ngOnInit() {
  }

    onSubmit(email, password, name, surname, username, homephonenumber, mobilenumber){
    this._userService.register(email, password, name, surname, username, homephonenumber, mobilenumber)
      .subscribe((result) => {
          if(result.access_token != null) {
            this._router.navigate(['']);
          }
          else
          {
            this.errorMessage = result.message;
          }
    });
  }
}

