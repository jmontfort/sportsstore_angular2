//In our LoginComponent we listen to the result of the login and after a successful login, we redirect the user to the home page. At redirect we reference the route 
//by it’s path declared before.

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import {UserService} from 'app/services/user.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  errorMessage: string = "";

 constructor(private _userService:UserService, private _router:Router) { }

  ngOnInit() {
  }

  onSubmit(email, password){
    this._userService.login(email, password)
    .subscribe((result) => {
      if(result.access_token != null) {
        this._router.navigate(['']);
      }
      else
      {
        this.errorMessage = result.message;
      }
    });
  }

}
