//For each route definition we can restrict access by creating a guard and adding it to the canActivate property.
//The new guard uses the previously defined isLoggedIn method from the UserService to determine whether the user is allowed to see the page or not. 
//It needs to implement the CanActivate interface to become usable by the route definition. In this example we are returning a simple boolean value.

import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

import { UserService } from '../app/services/user.service';

@Injectable()
export class LoggedInGuard implements CanActivate {
    constructor(private _userService:UserService) {}

    canActivate() {
        console.log('authTokenFromCanActivate()' + localStorage.getItem('auth_token'));
        return this._userService.isLoggedIn();
    }
}
