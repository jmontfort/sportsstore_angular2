
import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Observable';


@Injectable()
export class UserService {
  private loggedIn = false;
  apiUrl: string;

  constructor(private _http:Http) { 
    this.loggedIn = !!localStorage.getItem('auth_token');
    this.apiUrl = 'http://localhost:60000/api/';
  }


  login(email, password) {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      let credentials = JSON.stringify({ email: email, password: password });
       //return this._http.post(this.apiUrl + 'Account/Login', credentials, { headers: headers })
       return this._http.post(this.apiUrl + 'Account/Login', credentials, { headers: headers })
        .map(res => res.json())
        .map((res) => {
          if (res.access_token != null) {
            localStorage.setItem('auth_token', res.access_token);
            this.loggedIn = true;
          }
          return res;
        })
        .catch(this.handleError);
    }

    register(email, password, name, surname, username, homephonenumber, mobilenumber){
      let headers = new Headers();
      let body = JSON.stringify({ email:email, password:password, name:name, surname:surname, username:username, homeNo:homephonenumber, mobno:mobilenumber})
      headers.append('Content-Type', 'application/json');
      return this._http.post(this.apiUrl + 'Account/Register', body, {headers: headers })
        .map(res => res.json())
        .map((res) => {          
          if (res.access_token != null) {
            localStorage.setItem('auth_token', res.access_token);
            this.loggedIn = true;
          }
          return res;
        })
        .catch(this.handleError);
    }

    logout() {
      localStorage.removeItem('auth_token');
      this.loggedIn = false;
    }

    isLoggedIn() {
      return this.loggedIn;
    }

    public handleError(error: Response) {
      console.log('we have an error');
      console.error(error);
      return Observable.throw(error.json().error || 'Server error');
  }
}
