import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Observable';
import {Category} from '../Classes/Category';
import {Product} from '../Classes/Product';
import {Router} from '@angular/router';

@Injectable()
export class CategoriesService {
  apiUrl: string;
  categories: Category[] = [];
  products: Product[] = [];

  constructor(private _http:Http, private router:Router) { 
    this.apiUrl = 'http://localhost:60000/api/';
  }

  getCategories() {
    let headers = new Headers();
    headers.append('Content-type', 'application/json');

    let authToken = localStorage.getItem('auth_token');
    headers.append('Authorization', `Bearer ${authToken}`);
    return this._http.get(this.apiUrl+ 'Categories/Get', { headers: headers, withCredentials: true })
      .map(this.extractCategory)
      .catch(error => this.handleError(error, this.router));
  }


  search(str: string){
    let headers = new Headers();
    headers.append('Content-type', 'application/json');

    let authToken = localStorage.getItem('auth_token');
    headers.append('Authorization', `Bearer ${authToken}`);
    if(str == ""){
      return this._http.get(this.apiUrl+ 'Categories/Get', { headers: headers, withCredentials: true })
        .map(this.extractCategory)
        .catch(error => this.handleError(error, this.router));
    }
    else{
      return this._http.get(this.apiUrl+ 'Categories/Get/name='+str, { headers: headers, withCredentials: true })
        .map(this.extractCategory)
        .catch(error => this.handleError(error, this.router));

    }

  }

    checkExistingProducts(id:number){
      let headers = new Headers();
      headers.append('Content-type', 'application/json');

      let authToken = localStorage.getItem('auth_token');
      headers.append('Authorization', `Bearer ${authToken}`);
      return this._http.get(this.apiUrl+ 'Categories/CheckExistingCategoryProduct/' + id, { headers: headers, withCredentials: true })
        .map(this.extractProduct)
        .catch(error => this.handleError(error, this.router));
      
    }


   private extractCategory(res: Response): Category {
    let categories = res.json();
    return categories || { };
  }

   private extractProduct(res: Response): Product {
    let products = res.json();
    return products || { };
  }

   private handleError (error: any, router:Router) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
      if (error.status === 401 || error.status === 403) {
        //handle authorization errors
        this.router.navigate(['notAuthorized']);
      }

    return Observable.throw(errMsg);
  }
  

}
