import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, Http, RequestOptions } from '@angular/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';

import { UserService } from '../app/services/user.service';
import { CategoriesService } from '../app/services/categories.service';

import { routing } from '../app/app.routes';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoggedInGuard } from '../app/logged-in.guard';
import { LogoutComponent } from './components/logout/logout.component';
import { CategoriesComponent } from '../app/components/categories/categories.component';
import { BrandsComponent } from '../app/components/brands/brands.component';
import { ProductsComponent } from '../app/components/products/products.component';
import { UsersComponent } from '../app/components/users/users.component';
import { RegisterComponent } from './components/register/register.component';
import { NotAuthorizedComponent } from './components/not-authorized/not-authorized.component';

@NgModule({
  declarations: [AppComponent,
                  HomeComponent,
                  LoginComponent,
                  NavbarComponent, 
                  HomeComponent, 
                  LoginComponent, 
                  NavbarComponent, 
                  LogoutComponent, 
                  CategoriesComponent, 
                  BrandsComponent, 
                  ProductsComponent, 
                  UsersComponent, RegisterComponent, NotAuthorizedComponent],
  imports:      [ BrowserModule, FormsModule, HttpModule, routing ],
  providers: [UserService, CategoriesService, LoggedInGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
