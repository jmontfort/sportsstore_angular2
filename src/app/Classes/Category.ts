import { Injectable } from '@angular/core';

@Injectable()
export class Category{
    id: number;
    name: string;
}