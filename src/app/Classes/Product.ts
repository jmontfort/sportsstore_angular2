import { Brand } from 'app/Classes/Brand';
import { Category } from 'app/Classes/Category';
import { Image } from 'app/Classes/Image';

export class Product {
    id: number;
    name: string;
    description: string;
    price: number;
    stock: number;
    deal: boolean;
    discount: number;
    brand: Brand;
    category: Category;
    productImages: Image[] = [];
}




