import { Injectable } from '@angular/core';

@Injectable()
export class Image{
    id: number;
    name: string;
    url: string;
}
