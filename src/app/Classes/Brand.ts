import { Injectable } from '@angular/core';

import { Image } from 'app/Classes/Image';


@Injectable()
export class Brand{
    id: number;
    name: string;
    image: Image[];
}



